#include "cliente.h"

int Cliente::ultimoId = 0;

Cliente::Cliente(int tempId): id(tempId)
{

}

Cliente::Cliente(string tempNome, string tempContacto) : contacto(tempContacto), nome(tempNome)
{
	ultimoId++;
	id = ultimoId;
}

Cliente::Cliente()
{
	ultimoId++;
	id = ultimoId;
}

Cliente::Cliente(int tempId, string tempNome, string tempContacto) : contacto(tempContacto), nome(tempNome)
{
	if(tempId > ultimoId)
		ultimoId = tempId;
	// Se for menor devia-se verificar se j� existe...

	id = tempId;
}

ostream & operator<<(ostream &o, const Cliente &c)
{
	stringstream ss;
	ss << "Id: " << c.getId() << " | Nome: " << c.getNome() << " | Contacto: " << c.getContacto();

	o << center(ss.str());

	return o;
}