#include "produtor.h"

using namespace std;

int Produtor::ultimoId = 0;

Produtor::Produtor(int tempId, string tempNome, vector<Produto *> tempProdutos, vector<Uva *> tempUvas)
{
	if(tempId > ultimoId)
		ultimoId = tempId;
	//Devia-se verificar se existe repeti��o....

	id = tempId;
	nome = tempNome;
	produtos = tempProdutos;
	uvas = tempUvas;
}

Produtor::Produtor(string tempNome, vector<Produto *> tempProdutos, vector<Uva *> tempUvas)
{
	ultimoId++;
	id = ultimoId;

	nome = tempNome;
	produtos = tempProdutos;
	uvas = tempUvas;
}

Produtor::Produtor(string tempNome)
{
	nome=tempNome;

	ultimoId++;
	id = ultimoId;
}

Produtor::Produtor()
{
	ultimoId++;
	id = ultimoId;
}

bool Produtor::addProduto(Produto* produto)	{ 

	//Verificar se h� produtos DUPLICADOS
	for(unsigned int i = 0; i < produtos.size(); i++) {
		if( produto == produtos[i] )
			return false;
	}

	//Verificar se tem as uvas necess�rias
	if(temUvas( produto->getUvasVec() ) ) {
		produtos.push_back(produto); 
		return true;
	} else
		return false;
}

bool Produtor::temUvas(vector<Uva *> Uvas) const { 
	int found = 0; 
	for(unsigned int i = 0; i < Uvas.size(); i++) {
		for(unsigned int j = 0; j < uvas.size(); j++) {
			if(Uvas[i] == uvas[j]) {
				found++;
				break;
			}
		}
	}
	return (found == Uvas.size());
}

ostream & operator<<(ostream &o, Produtor &p)
{
	stringstream ss;
	ss << "Id: " << p.getId() 
	   << " | Nome: " << p.getNome()
	   << " | Uvas: ";

	vector<Uva *> tempUvas = p.getUvas();
	for(int j=0;j < tempUvas.size();j++)
     {
		 ss << tempUvas[j]->getId() << " ";
     }

	ss << " | Produtos: ";

	vector<Produto *> tempProdutos = p.getProdutos();
	for(int j=0;j < tempProdutos.size();j++)
     {
             ss << tempProdutos[j]->getId() << " ";
     }

	o << center(ss.str());

	return o;
}