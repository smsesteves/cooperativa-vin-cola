#ifndef PRODUTOR_H_INCLUDED
#define PRODUTOR_H_INCLUDED

#include <string>
#include <vector>

#include "produto.h"

using namespace std;


class Produtor {
    int id;
    static int ultimoId;
    string nome;
    vector<Produto *> produtos;
    vector<Uva *> uvas;
public:
   Produtor(int tempId, string tempNome, vector<Produto *> tempProdutos, vector<Uva *> tempUvas);
   Produtor(string tempNome, vector<Produto *> tempProdutos, vector<Uva *> tempUvas);
   Produtor(string tempNome);
   Produtor();

    /** @brief Retorna Id @return id **/
   int getId() const					{ return id; }
    /** @brief Retorna Nome @return nome **/
   string getNome() const				{ return nome; }
	/** @brief Retorna vector com as uvas @return vector das uvas do produtor **/
   vector<Uva *> getUvas() const		{ return uvas; }
    /** @brief Retorna tamanho do vector uvas @return tamanho do vector uvas **/
   int getUvasSize() const				{ return uvas.size(); }
    /** @brief Retorna Id da Uva na posicao dada @param Posicao da Uva @return Id da Uva na posicao dada **/
   int getUvaId(int pos) const			{ return uvas[pos]->getId(); }
	/** @brief Retorna vector com as produtos @return vector dos produtos do Produtor **/
   vector <Produto *> getProdutos() const { return produtos; }
    /** @brief Retorna tamanho do vector produtos @return tamanho do vector produtos **/
   int getProdSize() const				{ return produtos.size(); }
	/** @brief Retorna Id do Produto na posicao dada @param Posicao do Produto @return Id do Produto na posicao dada **/
   int getProdId(int pos) const			{ return produtos[pos]->getId(); }
    /** @brief Retorna Nome do Produto na posicao dada @param Posicao do Produto @return Id do Produto na posicao dada **/
   string getProdDes(int pos) const		{ return produtos[pos]->getNome(); }

   /**@brief Elimina produto da posicao dada @param pos **/
   void eliminarProd(int pos)			{ produtos.erase(produtos.begin()+pos); }
    /**@brief Elimina uva da posicao dada @param pos **/
   void eliminarUva(int pos)			{ uvas.erase(uvas.begin()+pos); }
   /**@brief Elimina Produto com id dado @param id @return true se encontra, false se nao **/
   bool eliminarProdId(int id)			{ for(unsigned int i = 0; i < produtos.size(); i++) if(produtos[i]->getId() == id) { produtos.erase(produtos.begin()+i); return true; } return false; }
   /**@brief Elimina uva com id dado @param id @return true se encontra, false se nao **/
   bool eliminarUvaId(int id)			{ for(unsigned int i = 0; i < uvas.size(); i++) if(uvas[i]->getId() == id) { uvas.erase(uvas.begin()+i); return true; } return false; }

   /**@brief Muda nome @param novo nome **/
   void setNome(string Nome)			{ nome = Nome; }

   /**@brief Adiciona Produto @param apontador para o produto @return true se possivel, false se nao **/
   bool addProduto(Produto* produto);
    /**@brief Adiciona Uva @param apontador para a uva **/
   void addUva(Uva* uva)				{ uvas.push_back(uva); }

   /**@brief Verifica se tem as Uvas @param vector de uvas @return true se sim, false se nao **/
   bool temUvas(vector<Uva *> Uvas) const;

   friend ostream & operator<<(ostream &o, Produtor &p);

};




#endif // PRODUTOR_H_INCLUDED
