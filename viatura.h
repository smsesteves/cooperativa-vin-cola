#ifndef VIATURA_H_INCLUDED
#define VIATURA_H_INCLUDED

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>

#include "utilidades.h"

using namespace std;

class Viatura {
	int id;
	double capacidade;
	static int ultimoId;
public:
	Viatura(double Capacidade);
	Viatura(int Id, double Capacidade);
	
	/** @brief Retorna Id @return id **/
	double getId() const { return id; }
	/**@brief Muda o Id @param novo id **/
	void setId(int Id)	 { id = Id; }

	/** @brief Retorna capacidade da viatura @return capacidade **/
	double getCapacidade() const	{ return capacidade; }
	/**@brief Muda a capacidade @param nova capacidade **/
	void setCapacidade(double Capacidade) { capacidade = Capacidade; }

	friend ostream & operator<<(ostream &o, Viatura &v);

};

#endif