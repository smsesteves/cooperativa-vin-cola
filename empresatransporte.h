#ifndef TRANSPORTE_H_INCLUDED
#define TRANSPORTE_H_INCLUDED

#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <cctype>
#include <list>

#include "viatura.h"
#include "utilidades.h"

using namespace std;

class EmpresaTransporte {
	int id;
	string nome;
	double distancia; //Distancia � cooperativa
	list <Viatura> viaturas;
	static int ultimoId;
public:
	EmpresaTransporte() { }
	EmpresaTransporte(string Nome, double Distancia, list <Viatura> Viaturas);
	EmpresaTransporte(int Id, string Nome, double Distancia, list <Viatura> Viaturas);
	
	 
	/** @brief Retorna Id @return id **/
	int getId() const						{ return id; }
	/** @brief Retorna Nome da Empresa @return nome **/
	string getNome() const					{ return nome; }
	/** @brief Retorna distancia da empresa � cooperativa @return distancia **/
	double getDistancia() const				{ return distancia; }
	/** @brief Retorna a fila de Viaturas da Empresa @return list de Viaturas **/
	list <Viatura> getViaturas() const	{ return viaturas; }

	double getCapacidade() const;

	/**@brief Muda o Id @param novo id **/
	void setId(int Id)							{ id = Id; }
	/**@brief Muda nome @param novo nome **/
	void setNome(string Nome)					{ nome = Nome; }
	/**@brief Muda o local da empresa @param nova distancia **/
	void setLocal(double Distancia)					{ distancia = Distancia; }
	/**@brief Muda as viaturas da empresa @param nova fila de viaturas **/
	void setViaturas(list <Viatura> Viaturas) { viaturas = Viaturas; }

	/**@brief Adiciona uma viatura � lista das mesmas @param nova viatura **/
	void adicionaViatura(Viatura tmpViatura);
	/**@brief Remove a viatura de Id Id da fila de viaturas @param Id da viatura e remover @return booleano que verifica a remo��o **/
	bool removeViatura(int Id);

	/**@brief Compara distancias entre empresas @param Empresa a comparar @return booleano que verifica a compara��o **/
	bool operator<(const EmpresaTransporte & e2) const;

	/**@brief Retorna um ostream para que os dados sejam escritos na consola @param ostream e a Empresa de transporte a escrever @return ostream com os dados **/ 
	friend ostream & operator<<(ostream &o, EmpresaTransporte &c);

};

#endif