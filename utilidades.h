#ifndef UTILIDADES_H_INCLUDED
#define UTILIDADES_H_INCLUDED

#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <ctime>
#include <sstream>

using namespace std;

/** @brief Converte uma string num numero @param string a converter. @return Numero da string convertida .**/
template<typename T>
T StringToNumber(const std::string& numberAsString);

/** @brief Converte um inteiro numa string. @param int a converter. @return String do numero convertido .**/
string IntToString ( int Number );
/** @brief Converte um double numa string. @param doubel a converter. @return String do numero convertido .**/
string DoubleToString( double Number);


template<typename G>
string NumberToString(G number);

/** @brief Retira o id de uma string. @param string a cortar. @return id convertido .**/
int stringCutId(string& original);
/** @brief Retira o time de uma string. @param string a cortar. @return time convertido .**/
std::time_t stringCutTime(string& original);
/** @brief Retira o conteudo de uma string entre duas strings nela contidas. @param string a cortar, string inicial e final. @return conteudo convertido .**/
string stringCutConteudo(string& original, string inicial, string final);

/** @brief Retira os inteiros de uma string para um vector separados por determinado caracter. @param string a cortar, separador. @return vector com os inteiros.**/
vector<int> explode(string original, string separador);

/** @brief Retira os inteiros de uma string para um vector separados por determinados caracteres (2) . @param string a cortar, dois separadores. @return vector com os inteiros.**/
vector< vector<float> > explodeD(string original, string separador, string separador2);

/** @brief Centra um string no ecra. @param string a centrar, booleano caso queira fazer newline.**/
void stringCenter(string s, bool newline = true);

/** @brief Centra uma string e devolve-a. @string a centrar. @return string centrada **/
string center(string s);


#endif // UTILIDADES_H_INCLUDED